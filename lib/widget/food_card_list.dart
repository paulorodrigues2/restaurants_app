import 'package:flutter/material.dart';
import 'package:restaurant_qr/colorsfile.dart';
import 'package:restaurant_qr/widget/two_sided_rounded_button.dart';


class CartListCard extends StatelessWidget {
  final String image;
  final String title;
  final String auth;
  final double rate;
  final Function pressDetails;
  final Function pressAddOrder;
  const CartListCard({
    Key key,
    this.image,
    this.title,
    this.auth,
    this.rate,
    this.pressDetails,
    this.pressAddOrder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 24, bottom: 40),
      height: 225,
      width: 202,
      child: Stack(
        children: [
          Positioned(
            bottom: 0,
            left: 10,
            right: 0,
            child: Container(
              height: 221,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(29),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 10),
                    blurRadius: 33,
                    color: kShadowColor,
                  ),
                ],
              ),
            ),
          ),
          Image.asset(
            image,
            width: 200,
          ),
//
          Positioned(
            top: 140,
            child: Container(
              height: 85,
              width: 202,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 24),
                    child: RichText(
                      maxLines: 3,
                      text: TextSpan(
                        style: TextStyle(color: kBlackColor),
                        children: [
                          TextSpan(
                            text: "$title\n",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text: auth,
                            style: TextStyle(
                              color: kLightBlackColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Spacer(),
                  Row(
                    children: [
                      GestureDetector(
                        onTap: pressDetails,
                        child: Container(
                          width: 101,
                          padding: EdgeInsets.symmetric(vertical: 10),
                          alignment: Alignment.center,
                          child: Text("\t\t\t\t Rate: $rate \$",style: TextStyle(
                            fontWeight: FontWeight.bold
                          ),),
                        ),
                      ),
                      Expanded(
                        child: TwoSideRoundedButton(
                          text: "Order",
                          press: pressAddOrder,
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
