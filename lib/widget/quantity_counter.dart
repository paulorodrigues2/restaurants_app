import 'package:restaurant_qr/colorsfile.dart';
import 'package:flutter/material.dart';

class QuantityRate extends StatelessWidget {
  final int score;
  const QuantityRate({
    Key key,
    this.score,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding:
      EdgeInsets.symmetric(vertical: 8, horizontal: 6),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(16),
          boxShadow: [
            BoxShadow(
              offset: Offset(3, 7),
              blurRadius: 20,
              color: Color(0xFD3D3D3).withOpacity(.5),
            )
          ]),
      child: Row(
        children: [
          Icon(
            Icons.arrow_back_ios,
            color: kIconColor,
            size: 25,
          ),
          SizedBox(
            width: 8,
          ),
          Text(
            "$score",
            style: TextStyle(
                fontSize: 12, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            width: 8,
          ),
          Icon(
            Icons.arrow_forward_ios,
            color: kIconColor,
            size: 25,
          ),
        ],
      ),
    );
  }
}
