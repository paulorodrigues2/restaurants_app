import 'package:flutter/material.dart';

class CardFrontView extends StatelessWidget {
  final String cardNumber;
  final String cardHolderName;
  final String cardExpiry;


  String _formattedCardNumber;
  String _formattedExpiryDate;

  CardFrontView(
      {Key key, this.cardNumber, this.cardHolderName, this.cardExpiry})
      : super(key: key) {
    _formattedCardNumber = this.cardNumber.padRight(16, '*');
    _formattedCardNumber = _formattedCardNumber.replaceAllMapped(
        RegExp(r".{4}"), (match) => "${match.group(0)} ");


    _formattedExpiryDate =
  this.cardExpiry.replaceAllMapped(RegExp(r".{2}"), (match) => "${match.group(0)}/");
}
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 5,
      height: 300,
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16),),
        elevation: 8,
        margin: EdgeInsets.all(16),
        child: Padding(
          padding: const EdgeInsets.all(32.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.topRight,
                child: Image.asset('images/visa.png'),
              ),
              SizedBox(
                height: 32,
              ),
              Text(
                _formattedCardNumber,
                textAlign: TextAlign.start,
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    wordSpacing: 2,
                    fontSize: 24,
                    letterSpacing: 2,
                    shadows: [
                      Shadow(
                        color: Colors.black12,
                        offset: Offset(2, 1),
                      ),
                    ],),
              ),
              SizedBox(
                height: 32,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Card Holder'),
                        Text(
                          cardHolderName,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            letterSpacing: 2,
                              fontWeight: FontWeight.w500, fontSize: 18),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Expiry'),
                      Text(

                        _formattedExpiryDate,
                        style: TextStyle(
                          letterSpacing: 2,
                            fontWeight: FontWeight.w500, fontSize: 18),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
