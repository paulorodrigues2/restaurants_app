import 'package:flutter/material.dart';
import 'package:restaurant_qr/widget/card_back.dart';
import 'package:restaurant_qr/widget/card_front.dart';
import 'dart:math';

class PaymentScreen extends StatefulWidget {
  @override
  _PaymentScreenState createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen>
    with SingleTickerProviderStateMixin {
  AnimationController _flipAnimationController;
  Animation<double> _flipAnimation;

  TextEditingController _cardNumberController, _cvvController, _cardHolderNameController,
  _cardExpiryController;

  FocusNode _cvvFocus;
  String _cardNumber = '';
  String _cardHolderName = '';
  String _cardExpiry = '';
  String _cvvNumber = '';

  _PaymentScreenState() {
    _cardNumberController = TextEditingController();
    _cardHolderNameController=TextEditingController();
    _cardExpiryController=TextEditingController();
    _cvvController = TextEditingController();

    _cvvFocus = FocusNode();

    _cardNumberController.addListener(onCardNumberChange);
//    _cardNumberController.addListener(() {
//      _cardNumber = _cardNumberController.text;
//      setState(() {});
//    });
    _cardHolderNameController.addListener(() {
      _cardHolderName=_cardHolderNameController.text;
      setState(() {});
    });
    _cardExpiryController.addListener(() {
      _cardExpiry = _cardExpiryController.text;
      setState(() {});
    });

    _cvvController.addListener(() {
      _cvvNumber = _cvvController.text;
      setState(() {});
    });
    _cvvFocus.addListener(() {
      _cvvFocus.hasFocus
          ? _flipAnimationController.forward()
          : _flipAnimationController.reverse();
    });
  }

  @override
  void initState() {
    super.initState();
    _flipAnimationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 350)
    );
    _flipAnimation =
        Tween<double>(begin: 0, end: 1).animate(_flipAnimationController)
          ..addListener(() {
            setState(() {});
          });
   // _flipAnimationController.forward();
  }
  void onCardNumberChange(){
    _cardNumber=_cardNumberController.text;
    setState(() {
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pay your bill"),
      ),
      body: Center(
        child: Column(
          children: [
            Transform(
              transform: Matrix4.identity()
                ..setEntry(3, 2, 0.001)
                ..rotateY(pi * _flipAnimation.value),
              origin: Offset(MediaQuery.of(context).size.width / 2, 0),
              child: _flipAnimation.value < 0.5
                  ? CardFrontView(
                      cardNumber: _cardNumber,
                cardHolderName: _cardHolderName,
                cardExpiry: _cardExpiry,
                    )
                  : CardBackView(
                      cvvNumber: _cvvNumber,
                    ),
            ),

            Expanded(
              flex: 1,
              child: Container(
                padding: EdgeInsets.all(16),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      //            Slider(
//                value: _rotationFactor,
//                onChanged: (double value) {
//                  setState(() {
//                    _rotationFactor = value;
//                  });
//                }),
                      TextField(
                        decoration: InputDecoration(hintText: 'Card Number'),
                        maxLength: 16,
                        controller: _cardNumberController,
                      ),
                      TextField(
                        controller: _cardHolderNameController,
                        decoration: InputDecoration(hintText: 'Name on Card'),
                      ),
                      Row(
                        children: [
                          Expanded(
                              flex: 2,
                              child: TextField(
                                maxLength: 4,
                                controller: _cardExpiryController,
                                decoration: InputDecoration(hintText: 'Expiry'),
                              )),
                          SizedBox(
                            width: 32,
                          ),
                          Expanded(
                            flex: 1,
                            child: TextField(
                              decoration: InputDecoration(hintText: 'CVV'),
                              controller: _cvvController,
                              maxLength: 3,
                              focusNode: _cvvFocus,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height:70,),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) {
                                return PaymentScreen();
                              },
                            ),
                          );
                        },
                        child: Container(
                            padding: EdgeInsets.symmetric(vertical: 16),
                            decoration: BoxDecoration(
                                color: Colors.blue,
                                borderRadius: BorderRadius.circular(30)),
                            width: MediaQuery.of(context).size.width - 40,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "Payment",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 18),
                                ),
                                Icon(
                                  Icons.arrow_forward,
                                  color: Colors.white,
                                )
                              ],
                            )),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
