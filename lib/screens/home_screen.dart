import 'package:restaurant_qr/screens/detail_screen.dart';
import 'package:restaurant_qr/widget/food_card_list.dart';
import 'package:flutter/material.dart';
import 'package:restaurant_qr/widget/two_sided_rounded_button.dart';

import '../colorsfile.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: double.infinity,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("images/main_page_bg.png"),
                  alignment: Alignment.topCenter,
                  fit: BoxFit.fitWidth,
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: size.height * 0.08,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                    ),
                    child: RichText(
                      text: TextSpan(
                        style: Theme.of(context).textTheme.display1,
                        children: [
                          TextSpan(
                              text: "Hyy ! Order Now.. \n",
                              style: TextStyle(
                                fontSize: 30,
                              )),
                          TextSpan(
                            text: "Menu List",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                    ),
                    child: RichText(
                      text: TextSpan(
                        style: TextStyle(color: Colors.redAccent.withOpacity(1)),
                        children: [
                          TextSpan(
                              text: "Burger ",
                              style: TextStyle(
                                fontSize: 25,
                              )),
                          TextSpan(
                            text: "Deals",
                            style: TextStyle(fontWeight: FontWeight.bold,fontSize: 25),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 25,),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        CartListCard(
                          image: "images/burger.png",
                          title: "Zinger Bugrer",
                          auth: "Spicy Burger with double Petty Chicken",
                          rate: 20,
                          pressAddOrder: () {},
                        ),
                        CartListCard(
                          image: "images/burger.png",
                          title: "Zinger Bugrer",
                          auth: "Spicy Burger with double Petty Chicken",
                          rate: 20,
                          pressAddOrder: () {},

                        ),
                        CartListCard(
                          image: "images/burger.png",
                          title: "Zinger Bugrer",
                          auth: "Spicy Burger with double Petty Chicken",
                          rate: 20,
                          pressAddOrder: () {},

                        ),
                        CartListCard(
                          image: "images/burger.png",
                          title: "Zinger Bugrer",
                          auth: "Spicy Burger with double Petty Chicken",
                          rate: 20,
                          pressAddOrder: () {},

                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                    ),
                    child: RichText(
                      text: TextSpan(
                        style: TextStyle(color: Colors.redAccent.withOpacity(1)),
                        children: [
                          TextSpan(
                              text: "Starter ",
                              style: TextStyle(
                                fontSize: 25,
                              )),
                          TextSpan(
                            text: "Deals",
                            style: TextStyle(fontWeight: FontWeight.bold,fontSize: 25),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 25,),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        CartListCard(
                          image: "images/burger.png",
                          title: "Zinger Bugrer",
                          auth: "Spicy Burger with double Petty Chicken",
                          rate: 20,
                          pressAddOrder: () {},
                        ),
                        CartListCard(
                          image: "images/burger.png",
                          title: "Zinger Bugrer",
                          auth: "Spicy Burger with double Petty Chicken",
                          rate: 20,
                          pressAddOrder: () {},

                        ),
                        CartListCard(
                          image: "images/burger.png",
                          title: "Zinger Bugrer",
                          auth: "Spicy Burger with double Petty Chicken",
                          rate: 20,
                          pressAddOrder: () {},

                        ),
                        CartListCard(
                          image: "images/burger.png",
                          title: "Zinger Bugrer",
                          auth: "Spicy Burger with double Petty Chicken",
                          rate: 20,
                          pressAddOrder: () {},

                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                    ),
                    child: RichText(
                      text: TextSpan(
                        style: TextStyle(color: Colors.redAccent.withOpacity(1)),
                        children: [
                          TextSpan(
                              text: "Main ",
                              style: TextStyle(
                                fontSize: 25,
                              )),
                          TextSpan(
                            text: "Course",
                            style: TextStyle(fontWeight: FontWeight.bold,fontSize: 25),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 25,),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        CartListCard(
                          image: "images/burger.png",
                          title: "Zinger Bugrer",
                          auth: "Spicy Burger with double Petty Chicken",
                          rate: 20,
                          pressAddOrder: () {},
                        ),
                        CartListCard(
                          image: "images/burger.png",
                          title: "Zinger Bugrer",
                          auth: "Spicy Burger with double Petty Chicken",
                          rate: 20,
                        ),
                        CartListCard(
                          image: "images/burger.png",
                          title: "Zinger Bugrer",
                          auth: "Spicy Burger with double Petty Chicken",
                          rate: 20,
                        ),
                        CartListCard(
                          image: "images/burger.png",
                          title: "Zinger Bugrer",
                          auth: "Spicy Burger with double Petty Chicken",
                          rate: 20,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.grey.withOpacity(0.50),
        onPressed: ()
        {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) {
                return DetailsScreen();
              },
            ),
          );
        },
        tooltip: 'Order list',
        child: Icon(Icons.shopping_cart),
      ),
    );
  }
}
