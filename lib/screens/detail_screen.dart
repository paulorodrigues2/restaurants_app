import 'package:flutter/material.dart';
import 'package:restaurant_qr/colorsfile.dart';
import 'package:restaurant_qr/screens/home_screen.dart';
import 'package:restaurant_qr/screens/payment_screen.dart';
import 'package:restaurant_qr/widget/quantity_counter.dart';

class DetailsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
            appBar: AppBar(
        title: Text("Order"),
        backgroundColor: Colors.grey,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              alignment: Alignment.topCenter,
              children: [
                Container(
                  height: size.height * .5 - 270,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("images/bg.png"),
                      fit: BoxFit.fitWidth,
                    ),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(50),
                      bottomRight: Radius.circular(50),
                    ),
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 19),
                    child: Column(
                      children: [
                        SizedBox(
                          height: size.height * .014,
                        ),
                        CartScreenHeader()
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsetsDirectional.only(top: size.height * .2),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 24),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            RichText(
                              text: TextSpan(
                                  style: Theme.of(context).textTheme.display1,
                                  children: [
                                    TextSpan(
                                        text: "Your Order ",
                                        style: TextStyle(
                                          fontSize: 25,
                                        )),
                                    TextSpan(
                                      text: "List :",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 25),
                                    ),
                                  ]),
                            ),
                            bestOfTheDayCard(size, context),
                            bestOfTheDayCard(size, context),
                            bestOfTheDayCard(size, context),
                            SizedBox(
                              height: 5,
                            ),
                            Container(
                                padding: EdgeInsets.symmetric(vertical: 16),
                                width: MediaQuery.of(context).size.width - 40,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(right: 8.0),
                                      child: Text(
                                        //"Total: $TotalPrice \$",
                                        "Total: 302 \$",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black,
                                            fontSize: 18),
                                      ),
                                    ),
                                  ],
                                )),
                            SizedBox(
                              height: 10,
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) {
                                      return PaymentScreen();
                                    },
                                  ),
                                );
                              },
                              child: Container(
                                  padding: EdgeInsets.symmetric(vertical: 16),
                                  decoration: BoxDecoration(
                                      color: Colors.blue,
                                      borderRadius: BorderRadius.circular(30)),
                                  width: MediaQuery.of(context).size.width - 40,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Payment",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 18),
                                      ),
                                      Icon(
                                        Icons.arrow_forward,
                                        color: Colors.white,
                                      )
                                    ],
                                  )),
                            )
                          ],
                        ),
                      ),
//
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ],
            ), //            Padding(
//
            SizedBox(
              height: 40,
            )
          ],
        ),
      ),
    );
  }

  bestOfTheDayCard(Size size, BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      width: double.infinity,
      height: 205,
      child: Stack(
        children: [
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              padding:
                  EdgeInsets.only(left: 24, top: 24, right: size.width * .35),
              height: 165,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: Color(0xFFEAEAEA).withOpacity(.43),
                borderRadius: BorderRadius.circular(29),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
//
                  Text(
                    "Zinger Burger",
                    style: Theme.of(context).textTheme.title,
                  ),
                  Text(
                    "Spicy Burger with double Petty Chicken",
                    style: TextStyle(
                      color: kLightBlackColor,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      QuantityRate(score: 1),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Text(
                          "Quantity !",
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 15,
                            color: kLightBlackColor,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            right: -27,
            top: -7,
            child: Image.asset(
              "images/burger.png",
              width: size.width * .50,
            ),
          ),
          Positioned(
              bottom: 35,
              right: 20,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30)),
                alignment: Alignment.center,
                width: 120,
                child: Text(
                  "Price: 20 \$",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                ),
              ))
//
        ],
      ),
    );
  }
}

class ChapterCard extends StatelessWidget {
  final String name;
  final String tag;
  final int chapterNumber;
  final Function press;

  const ChapterCard({
    Key key,
    this.name,
    this.tag,
    this.chapterNumber,
    this.size,
    this.press,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.symmetric(vertical: 15, horizontal: 25),
      margin: EdgeInsets.only(bottom: 16),
      width: size.width - 40,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(38.5),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 10),
            blurRadius: 33,
            color: Color(0xFFD3D3D3).withOpacity(.84),
          )
        ],
      ),
      child: Row(
        children: [
          RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: "Step $chapterNumber : $name \n",
                  style: TextStyle(
                    fontSize: 16,
                    color: kBlackColor,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                TextSpan(
                  text: tag,
                  style: TextStyle(color: kLightBlackColor),
                ),
              ],
            ),
          ),
          Spacer(),
          IconButton(
            icon: Icon(
              Icons.arrow_forward_ios,
              size: 18,
            ),
            onPressed: press,
          )
        ],
      ),
    );
  }
}

class CartScreenHeader extends StatelessWidget {
  const CartScreenHeader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Positioned(
          right: 10,
          left: 10,
          child: Container(
            child: Row(
              children: [
              ],
            ),
          ),
        ),
        SizedBox(
          width: 30,
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Confirm your",
                style: Theme.of(context).textTheme.display1,
              ),
              Text(
                "Order",
                style: Theme.of(context)
                    .textTheme
                    .display1
                    .copyWith(fontWeight: FontWeight.bold),
              ),
//
            ],
          ),
        ),
        Image.asset(
          'images/mm2.png',
          height: 100,
        ),
      ],
    );
  }
}
